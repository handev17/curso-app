import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowNewServer = false;
  serverCreationStatus = 'No server was created!';
  serverName='';
  serverRoute='';
  serverCreated = false;

  constructor() { 
    setTimeout(() => {
      this.allowNewServer = true;
    } , 2000);
  }

  ngOnInit() {
  }

  onCreateServer(){
    this.serverCreated = true;
    /* this.serverCreationStatus = 'Server was created! Name is '+this.serverName+' and route is '+this.serverRoute; */
  }
  onUpdateServerName(event: any){
    console.log(event);
    this.serverName = (<HTMLInputElement>event.target).value;


  }

}
